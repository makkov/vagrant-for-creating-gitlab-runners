#!/bin/bash
export REGISTRATION_TOKEN=${REGISTRATION_TOKEN}
export URL=${URL:-https://gitlab.com/}
export DOCKER_IMAGE=${DOCKER_IMAGE:-python:3.9}

sudo gitlab-runner register -n \
  --name "${DOCKER_IMAGE}-docker-runner" \
  --url https://gitlab.com/ \
  --registration-token $REGISTRATION_TOKEN \
  --tag-list docker-runner \
  --maintenance-note docker-runner \
  --executor docker \
  --docker-image $DOCKER_IMAGE
