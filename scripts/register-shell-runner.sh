#!/bin/bash
export REGISTRATION_TOKEN=${REGISTRATION_TOKEN}
export URL=${URL:-https://gitlab.com/}

sudo gitlab-runner register -n \
  --name shell-runner \
  --url $URL \
  --registration-token $REGISTRATION_TOKEN \
  --tag-list shell-runner \
  --maintenance-note shell-runner \
  --executor shell
