# vagrant-for-creating-gitlab-runners



## About project

Project contains Vagrant file with scripts for creating and registration gitlab-runner (shell and docker) for VM on ubuntu/focal64.


## Getting started
1. Create directory "data" (for synced with directory "vagrant_data" on VM)
2. Install [vagrant-env plugin](https://github.com/gosuri/vagrant-env)
3. Configure file ".env". It contains parameters:
  * REGISTRATION_TOKEN - required param, you can see it value in Gitlab -> Setting -> CI/CD -> Runners
  * URL - additional param, if url differs from https://gitlab.com/ (default value)
  * DOCKER_IMAGE - additional param (python:3.9 by default). Docker image for docker runner
4. For creating VM and registration runners execute command **vagrant up**
